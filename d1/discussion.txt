Git Commands:

mkdir	make directory
	ex. mkdir Adobofolder
cd 		change directory
	ex. cd Adobofolder
cd ..	back
pwd 	print working directory
ls		list
touch	create file
	ex. touch name.txt

=====
SSH Key
=====

1. open a Terminal
	window open "terminal" program

2. Create SSH Key
	Terminal type: ssh-keygen (go to root first)
	wait for enter file and blinking cursor. enter. enter
	note: passphrase = password
	wait for a random image
	Your identification has been saved in C:\Users\Jejomar Panes/.ssh/id_rsa.
	open with sublime. copy and paste to ssh key in gitlab profile

3. config git account in the device. global
	email
	ex. git config --global user.email "panes.jejomar@gmail.com"

	user name
	ex. git config --global user.name "Jejomar Panes"

	project email
	ex. git config --user.email "panes.jejomar@gmail.com"
	/* same with global email */

	to see name email and name (credentials)
	git config --global --list

4. 















